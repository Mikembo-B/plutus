package fr.umlv.plutus.feature_wallet.domain.use_case.deals

import fr.umlv.plutus.feature_wallet.domain.repository.DealRepository

class GetLastDealId(
    private val repository: DealRepository
) {
    suspend operator fun invoke(): Int? {
        return repository.getLastDealId()
    }
}