package fr.umlv.plutus.feature_wallet.presentation.deals

import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import fr.umlv.plutus.feature_wallet.domain.model.Deal
import fr.umlv.plutus.feature_wallet.domain.model.Tag
import fr.umlv.plutus.feature_wallet.domain.use_case.PlutusUseCases
import fr.umlv.plutus.feature_wallet.presentation.search.SearchEvent
import fr.umlv.plutus.feature_wallet.presentation.search.SearchViewModel
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class DealsViewModel @Inject constructor(
    private val plutusUseCases: PlutusUseCases, savedStateHandle: SavedStateHandle
) : ViewModel() {
    private val _state = mutableStateOf(DealsState())
    private val _dealName = mutableStateOf(
        DealTextFieldState(
            hint = "Enter name..."
        )
    )
    private val _dealAmount = mutableStateOf(0)
    private val _dealDate = mutableStateOf(0L)
    private val _walletName = mutableStateOf("")
    private val _tagList = mutableStateOf(emptyList<Tag>())
    private val _eventFlow = MutableSharedFlow<UiEvent>()
    private val _lastDealId = mutableStateOf(-1)
    private val _tagTitle = mutableStateOf("")
    private val _tagType = mutableStateOf("")
    var walletId: Int = 0

    val state: State<DealsState> = _state
    val dealName: State<DealTextFieldState> = _dealName
    val dealAmount: State<Int> = _dealAmount
    val dealDate: State<Long> = _dealDate
    val tagList: State<List<Tag>> = _tagList
    val tagTitle: State<String> = _tagTitle
    val tagType: State<String> = _tagType
    val walletName: State<String> = _walletName
    val lastDealId: State<Int> = _lastDealId

    private var getDealsJob: Job? = null

    init {
        savedStateHandle.get<Int>("walletId")?.let { id ->
            if (id != -1) {
                getDeals(id)
                viewModelScope.launch {
                    plutusUseCases.getWallet(id)?.also { wallet ->
                        _walletName.value = wallet.name
                    }
                }
            }
            walletId = id
        }
        plutusUseCases.getTags().onEach { list ->
            _tagList.value = list.distinctBy { it.title }
        }.launchIn(viewModelScope)
        viewModelScope.launch {
            plutusUseCases.getLastDealId()?.also { dealId ->
                _lastDealId.value = dealId
            }
        }
    }

    fun onEvent(event: DealsEvent) {
        when (event) {
            is DealsEvent.DeleteDeal -> {
                viewModelScope.launch {
                    plutusUseCases.deleteDeal(event.deal)
                }
            }
            is DealsEvent.EnteredName -> {
                _dealName.value = dealName.value.copy(
                    text = event.value
                )
            }
            is DealsEvent.EnteredAmount -> {
                _dealAmount.value = event.value
            }
            is DealsEvent.EnteredDate -> {
                _dealDate.value = event.value
            }
            is DealsEvent.SaveDeal -> {
                viewModelScope.launch {
                    plutusUseCases.addDeal(
                        Deal(
                            name = dealName.value.text,
                            amount = dealAmount.value,
                            date = dealDate.value,
                            idWallet = walletId
                        )
                    )
                    plutusUseCases.getLastDealId()?.also { dealId ->
                        _lastDealId.value = dealId
                    }
                    _eventFlow.emit(UiEvent.SaveDeals)
                }
            }
            is DealsEvent.EnteredTitle -> {
                _tagTitle.value = event.value
            }
            is DealsEvent.EnteredType -> {
                _tagType.value = event.value
            }
            is DealsEvent.SaveTag -> {
                viewModelScope.launch {
                    plutusUseCases.addTag(
                        Tag(
                            title = tagTitle.value,
                            type = tagType.value,
                            idDeal = lastDealId.value
                        )
                    )
                    _eventFlow.emit(UiEvent.SaveTag)
                }
            }
            is DealsEvent.UpdateDeal -> {
                viewModelScope.launch {
                    plutusUseCases.updateDeal(
                        Deal(
                            id = event.deal.id,
                            name = dealName.value.text,
                            amount = dealAmount.value,
                            date = dealDate.value,
                            idWallet = event.deal.idWallet
                        )
                    )
                }
            }
        }
    }

    private fun getDeals(walletId: Int) {
        getDealsJob?.cancel()
        getDealsJob = plutusUseCases.getDeals(idWallet = walletId).onEach { deals ->
            _state.value = state.value.copy(
                deals = deals
            )
        }.launchIn(viewModelScope)
    }

    sealed class UiEvent {
        object SaveDeals : UiEvent()
        object SaveTag : UiEvent()
    }
}