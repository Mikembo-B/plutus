package fr.umlv.plutus.feature_wallet.data.repository

import fr.umlv.plutus.feature_wallet.data.data_source.DealDao
import fr.umlv.plutus.feature_wallet.domain.model.Deal
import fr.umlv.plutus.feature_wallet.domain.repository.DealRepository
import kotlinx.coroutines.flow.Flow

class DealRepositoryImpl(
    private val dao: DealDao
) : DealRepository {
    override fun getDealsByIdWallet(idWallet: Int): Flow<List<Deal>> {
        return dao.getDealsByIdWallet(idWallet)
    }

    override fun getDealsByIds(ids: Array<Int>): Flow<List<Deal>> {
        return dao.getDealsByIds(ids)
    }

    override suspend fun getLastDealId(): Int? {
        return dao.getLastDealId()
    }

    override suspend fun insertDeal(deal: Deal) {
        dao.insertDeal(deal)
    }

    override suspend fun deleteDeal(deal: Deal) {
        dao.deleteDeal(deal)
    }

    override suspend fun updateDeal(deal: Deal) {
        dao.updateDeal(deal)
    }
}