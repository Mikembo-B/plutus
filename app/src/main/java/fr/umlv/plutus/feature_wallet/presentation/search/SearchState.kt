package fr.umlv.plutus.feature_wallet.presentation.search

import fr.umlv.plutus.feature_wallet.domain.model.Deal

data class SearchState(
    val deals: List<Deal> = emptyList()
)