package fr.umlv.plutus.feature_wallet.presentation.search

import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Home
import androidx.compose.material.icons.filled.Info
import androidx.compose.material.icons.outlined.ArrowDropDown
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.compose.ui.window.Dialog
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import fr.umlv.plutus.feature_wallet.domain.model.Tag

import fr.umlv.plutus.feature_wallet.presentation.deals.*
import fr.umlv.plutus.feature_wallet.presentation.deals.components.DealItem
import fr.umlv.plutus.feature_wallet.presentation.util.Screen

import java.util.*


@OptIn(ExperimentalAnimationApi::class)
@Composable
fun SearchScreen(
    navController: NavController,
    viewModel: SearchViewModel = hiltViewModel(),

    ){
    val state = viewModel.state.value
    val scaffoldState = rememberScaffoldState()
    val openDialog = remember { mutableStateOf(false) }

    Scaffold(
        scaffoldState = scaffoldState,
        topBar = {
            TopAppBar(title = { Text("Recherche par Tags") }, backgroundColor = Color.Blue, contentColor = Color.White)
        },
        floatingActionButtonPosition = FabPosition.End,
        floatingActionButton = {
            ShowAddTag(
                viewModel = viewModel,
                openDialog = openDialog
            )
            FloatingActionButton(
                onClick = {
                    openDialog.value = true
                },
            ) {
                Text("+")
            }
        },
        content = {
            TagsSpinner(viewModel = viewModel,
            )
            ScrollableFound(
                viewModel = viewModel,
                state = state,
                navController = navController
            )
        },
        bottomBar = {
            BottomAppBar(
                backgroundColor = Color.Blue,
                contentColor = Color.White
            ) {
                Row(
                    modifier = Modifier.fillMaxWidth()
                ) {
                    IconButton(modifier = Modifier
                        .weight(1f),
                        onClick = {
                            navController.navigate(Screen.WalletsScreen.route)
                        }
                    ) {
                        Icon(
                            imageVector = Icons.Filled.Home,
                            contentDescription = "Home"
                        )
                    }

                    IconButton(modifier = Modifier
                        .weight(1f),
                        onClick = {
                            navController.navigate(Screen.SearchScreen.route)
                        }
                    ) {
                        Icon(
                            imageVector = Icons.Filled.Info,
                            contentDescription = "Tag"
                        )
                    }
                }
            }
        }
    )
}


@Composable
fun TagsSpinner(
    viewModel: SearchViewModel,
    //onSelectionChanged: (selection: String) -> Unit
) {

    val tags = viewModel.tagList.value
    var TagVal = remember { mutableStateOf(Tag(type = "=", title ="Tag")) }
    var expanded by remember { mutableStateOf(false) }

    Spacer(modifier = Modifier.padding(10.dp))

    Box (
        modifier = Modifier.padding(15.dp)
    ){
        Column {
            OutlinedTextField(
                value = (TagVal.value.type+ " " +TagVal.value.title),
                onValueChange = { },
                label = { Text(text = "Séléctionner un tag") },
                modifier = Modifier.fillMaxWidth(),
                trailingIcon = { Icon(Icons.Outlined.ArrowDropDown, null) },
                readOnly = true
            )
            DropdownMenu(
                modifier = Modifier.fillMaxWidth(),
                expanded = expanded,
                onDismissRequest = { expanded = false },
            ) {
                tags.forEach { entry ->

                    DropdownMenuItem(
                        modifier = Modifier.fillMaxWidth(),
                        onClick = {
                            TagVal.value = entry
                            expanded = false
                        },
                        content = {
                            Text(
                                text = entry.type+""+entry.title,
                                modifier = Modifier.wrapContentWidth())
                        }
                    )
                }
            }
        }

        Spacer(
            modifier = Modifier
                .matchParentSize()
                .background(Color.Transparent)
                .padding(10.dp)
                .clickable(
                    onClick = { expanded = !expanded }
                )
        )
    }
    Spacer(modifier = Modifier.padding(10.dp))

}

@ExperimentalAnimationApi
@Composable
fun ScrollableFound(
    viewModel: SearchViewModel,
    state: SearchState,
    navController: NavController,
) {
    LazyColumn(modifier = Modifier.fillMaxSize()) {
        items(state.deals) { deal ->
            DealItem(
                deal = deal,
                modifier = Modifier
                    .fillMaxWidth()
                /*.clickable {
                    navController.navigate(
                        Screen.DealsScreen.route +
                                "?dealId=${deal.id}"
                    )
                }*/,
                onDeleteClick = {
                    //viewModel.onEvent(DealsEvent.DeleteDeal(deal))
                }
            )
            Spacer(modifier = Modifier.height(16.dp))
        }
    }
}

@Composable
fun ShowAddTag(
    openDialog: MutableState<Boolean>,
    viewModel: SearchViewModel
) {
    val NameVal = remember { mutableStateOf("") }
    val TypeVal = remember { mutableStateOf("") }

    if (openDialog.value) {
        Dialog(onDismissRequest = { openDialog.value = false }) {
            Surface(
                modifier = Modifier
                    .width(300.dp)
                    .height(350.dp)
                    .padding(5.dp),
                shape = RoundedCornerShape(5.dp),
                color = Color.White
            ) {
                Column(
                    modifier = Modifier.padding(5.dp),
                    horizontalAlignment = Alignment.CenterHorizontally,
                    verticalArrangement = Arrangement.Center
                ) {
                    Spacer(modifier = Modifier.padding(5.dp))

                    Text(
                        text = "Ajouter un tag",
                        color = Color.Black,
                        fontWeight = FontWeight.Bold,
                        fontSize = 25.sp,
                        textAlign = TextAlign.Center
                    )

                    Text(
                        text = "Type des tags : \n - : dépense \n + : revenu \n = : compte où l'opération est faite",
                        color = Color.DarkGray,
                        /*fontWeight = FontWeight.Bold,*/
                        fontSize = 15.sp,
                        textAlign = TextAlign.Center
                    )

                    Spacer(modifier = Modifier.padding(10.dp))

                    Row() {
                        OutlinedTextField(
                            value = TypeVal.value,
                            onValueChange = {
                                TypeVal.value = it
                                //viewModel.onEvent(TagEvent.EnteredName(it))
                            },
                            label = { Text(text = "Type") },
                            placeholder = { Text(text = "...") },
                            singleLine = true,
                            modifier = Modifier.fillMaxWidth(0.3f)
                        )

                        Spacer(modifier = Modifier.padding(10.dp))

                        OutlinedTextField(
                            value = NameVal.value,
                            onValueChange = {
                                NameVal.value = it
                            },
                            label = { Text(text = "Nom du tag") },
                            placeholder = { Text(text = "...") },
                            singleLine = true,
                            modifier = Modifier.fillMaxWidth(0.7f)
                        )

                    }

                    Spacer(modifier = Modifier.padding(10.dp))

                    Row() {
                        Button(
                            onClick = {
                                openDialog.value = false

                            },
                            modifier = Modifier
                                .weight(0.5f)
                                .height(60.dp)
                                .padding(10.dp),
                            shape = RoundedCornerShape(5.dp),
                            colors = ButtonDefaults.buttonColors(Color.Red)
                        ) {
                            Text(
                                text = "Fermer",
                                color = Color.White,
                                fontSize = 12.sp
                            )
                        }

                        Button(
                            onClick = {
                                openDialog.value = false
                                viewModel.onEvent(SearchEvent.EnteredTitle(NameVal.value))
                                viewModel.onEvent(SearchEvent.EnteredType(TypeVal.value))
                                viewModel.onEvent(SearchEvent.SaveTag)
                            },
                            modifier = Modifier
                                .weight(0.5f)
                                .fillMaxWidth(0.5f)
                                .height(60.dp)
                                .padding(10.dp),
                            shape = RoundedCornerShape(5.dp),
                            colors = ButtonDefaults.buttonColors(Color.Green)
                        ) {
                            Text(
                                text = "Ajouter",
                                color = Color.White,
                                fontSize = 12.sp
                            )
                        }
                    }
                }
            }
        }
    }
}