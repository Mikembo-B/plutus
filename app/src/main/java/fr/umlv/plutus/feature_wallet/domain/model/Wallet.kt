package fr.umlv.plutus.feature_wallet.domain.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "wallet")
data class Wallet(
    @PrimaryKey(autoGenerate = true) val id: Int = 0,
    val name: String
) {
}