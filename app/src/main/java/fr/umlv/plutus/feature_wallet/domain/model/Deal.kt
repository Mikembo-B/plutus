package fr.umlv.plutus.feature_wallet.domain.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "deal")
data class Deal(
    @PrimaryKey(autoGenerate = true) val id: Int = 0,
    val name: String,
    val amount: Int,
    val date: Long,
    val idWallet: Int
) {
}