package fr.umlv.plutus.feature_wallet.data.data_source

import androidx.room.Database
import androidx.room.RoomDatabase
import fr.umlv.plutus.feature_wallet.domain.model.Deal
import fr.umlv.plutus.feature_wallet.domain.model.Tag
import fr.umlv.plutus.feature_wallet.domain.model.Wallet

@Database(
    entities = [Wallet::class, Deal::class, Tag::class],
    version = 1,
    exportSchema = false
)
abstract class PlutusDatabase : RoomDatabase() {
    abstract val walletDao: WalletDao
    abstract val dealDao: DealDao
    abstract val tagDao: TagDao

    companion object {
        const val DATABASE_NAME = "plutus.db"
    }
}