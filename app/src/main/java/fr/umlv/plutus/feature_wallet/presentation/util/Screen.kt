package fr.umlv.plutus.feature_wallet.presentation.util

sealed class Screen(
    val route: String
) {
    object WalletsScreen : Screen("wallets_screen")
    object DealsScreen : Screen("deals_screen")
    object SearchScreen : Screen("search_screen")
}
