package fr.umlv.plutus.feature_wallet.domain.use_case.wallets

import fr.umlv.plutus.feature_wallet.domain.model.Wallet
import fr.umlv.plutus.feature_wallet.domain.repository.WalletRepository
import kotlinx.coroutines.flow.Flow

class GetWallets(
    private val repository: WalletRepository
) {
    operator fun invoke(): Flow<List<Wallet>> {
        return repository.getWallets()
    }
}