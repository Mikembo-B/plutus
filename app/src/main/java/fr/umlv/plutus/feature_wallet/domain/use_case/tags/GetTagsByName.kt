package fr.umlv.plutus.feature_wallet.domain.use_case.tags

import fr.umlv.plutus.feature_wallet.domain.model.Tag
import fr.umlv.plutus.feature_wallet.domain.repository.TagRepository
import kotlinx.coroutines.flow.Flow

class GetTagsByName(
    private val repository: TagRepository
) {
    operator fun invoke(title: String): Flow<List<Tag>> {
        return repository.getTagsByName(title)
    }
}