package fr.umlv.plutus.feature_wallet.presentation.deals

import fr.umlv.plutus.feature_wallet.domain.model.Deal

data class DealsState(
    val deals: List<Deal> = emptyList()
)