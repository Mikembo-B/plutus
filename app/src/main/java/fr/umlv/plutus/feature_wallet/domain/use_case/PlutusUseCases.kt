package fr.umlv.plutus.feature_wallet.domain.use_case

import fr.umlv.plutus.feature_wallet.domain.use_case.deals.*
import fr.umlv.plutus.feature_wallet.domain.use_case.tags.*
import fr.umlv.plutus.feature_wallet.domain.use_case.wallets.*

data class PlutusUseCases(
    val addWallet: AddWallet,
    val getWallets: GetWallets,
    val getWallet: GetWallet,
    val deleteWallet: DeleteWallet,
    val addDeal: AddDeal,
    val deleteDeal: DeleteDeal,
    val getDeals: GetDeals,
    val addTag: AddTag,
    val deleteTag: DeleteTag,
    val getTagsByIdDeal: GetTagsByIdDeal,
    val updateWallet: UpdateWallet,
    val updateDeal: UpdateDeal,
    val getTagsByName: GetTagsByName,
    val getDealsByIds: GetDealsByIds,
    val getTags: GetTags,
    val getLastDealId: GetLastDealId
) {
}