package fr.umlv.plutus.feature_wallet.domain.use_case.deals

import fr.umlv.plutus.feature_wallet.domain.model.Deal
import fr.umlv.plutus.feature_wallet.domain.repository.DealRepository

class DeleteDeal(
    private val repository: DealRepository
) {
    suspend operator fun invoke(deal: Deal) {
        repository.deleteDeal(deal)
    }
}