package fr.umlv.plutus.feature_wallet.presentation.deals

import androidx.compose.ui.focus.FocusState
import fr.umlv.plutus.feature_wallet.domain.model.Deal
import fr.umlv.plutus.feature_wallet.presentation.search.SearchEvent

sealed class DealsEvent {
    data class DeleteDeal(val deal: Deal) : DealsEvent()
    data class EnteredName(val value: String) : DealsEvent()
    data class EnteredAmount(val value: Int) : DealsEvent()
    data class EnteredDate(val value: Long) : DealsEvent()
    data class ChangeNameFocus(val focusState: FocusState) : DealsEvent()
    data class EnteredTitle(val value: String) : DealsEvent()
    data class EnteredType(val value: String) : DealsEvent()
    object SaveTag : DealsEvent()
    object SaveDeal : DealsEvent()
    data class UpdateDeal(val deal: Deal) : DealsEvent()
}