package fr.umlv.plutus.feature_wallet.presentation

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import androidx.navigation.navArgument
import dagger.hilt.android.AndroidEntryPoint
import fr.umlv.plutus.feature_wallet.presentation.deals.DealsScreen
import fr.umlv.plutus.feature_wallet.presentation.search.SearchScreen
import fr.umlv.plutus.feature_wallet.presentation.util.Screen
import fr.umlv.plutus.feature_wallet.presentation.wallets.WalletsScreen
import fr.umlv.plutus.ui.theme.PlutusTheme

@AndroidEntryPoint
class MainActivity : ComponentActivity() {
    @ExperimentalAnimationApi
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            PlutusTheme {
                Surface(
                    color = MaterialTheme.colors.background
                ) {
                    val navController = rememberNavController()
                    NavHost(
                        navController = navController,
                        startDestination = Screen.WalletsScreen.route
                    ) {
                        composable(route = Screen.WalletsScreen.route) {
                            WalletsScreen(navController = navController)
                        }
                        composable(route = Screen.SearchScreen.route) {
                            SearchScreen(navController = navController)
                        }
                        composable(
                            route = Screen.DealsScreen.route +
                                    "?walletId={walletId}",
                            arguments = listOf(
                                navArgument(
                                    name = "walletId"
                                ) {
                                    type = NavType.IntType
                                    defaultValue = -1
                                },
                            )
                        ) {
                            DealsScreen(navController = navController)
                        }
                    }
                }
            }
        }
    }
}

