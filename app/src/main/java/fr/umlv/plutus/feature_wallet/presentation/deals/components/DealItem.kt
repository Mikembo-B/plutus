package fr.umlv.plutus.feature_wallet.presentation.deals.components

import android.app.DatePickerDialog
import android.widget.DatePicker
import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.compose.ui.window.Dialog
import androidx.hilt.navigation.compose.hiltViewModel
import fr.umlv.plutus.feature_wallet.domain.model.Deal
import fr.umlv.plutus.feature_wallet.presentation.deals.DealsEvent
import fr.umlv.plutus.feature_wallet.presentation.deals.DealsViewModel
import java.text.SimpleDateFormat
import java.util.*

@Composable
fun DealItem(
    deal: Deal,
    viewModel: DealsViewModel = hiltViewModel(),
    modifier: Modifier = Modifier,
    onDeleteClick: () -> Unit
) {
    val formatter = SimpleDateFormat("dd/MM/yyyy")
    val openDialog = remember { mutableStateOf(false) }

    Box(
        modifier = modifier
    ) {
        Card(
            shape = RoundedCornerShape(4.dp),
            backgroundColor = Color.LightGray,
            modifier = Modifier
                .fillMaxWidth()
                .padding(16.dp)
        ) {
            Row(
                horizontalArrangement = Arrangement.SpaceBetween,
                verticalAlignment = Alignment.CenterVertically
            ) {
                Column(
                ) {
                    Text(
                        deal.name, style = TextStyle(
                            color = Color.Black,
                            fontSize = 20.sp,
                            /*textAlign = TextAlign.Center*/
                        ), modifier = Modifier.padding(10.dp)
                    )
                    Text(

                        (deal.amount.toFloat()/100.00).toString(), style = TextStyle(
                            color = Color.Blue,
                            fontSize = 20.sp,
                            fontWeight = FontWeight.Bold,
                            /*textAlign = TextAlign.Center*/
                        ), modifier = Modifier.padding(10.dp)
                    )
                    Text(
                        formatter.format(deal.date), style = TextStyle(
                            color = Color.Black,
                            fontSize = 20.sp,
                            /*textAlign = TextAlign.Center*/
                        ), modifier = Modifier.padding(10.dp)
                    )
                }
                Column(
                ) {
                    ShowModifierDeal(
                        deal = deal,
                        openDialog = openDialog,
                        viewModel = viewModel
                    )

                    IconButton(
                        onClick = {
                            openDialog.value = true
                        }
                    ) {
                        Icon(
                            imageVector = Icons.Filled.Create,
                            contentDescription = "Modifier"
                        )
                    }

                    IconButton(
                        onClick = {
                            viewModel.onEvent(DealsEvent.EnteredName(deal.name))
                            viewModel.onEvent(DealsEvent.EnteredAmount(deal.amount))
                            viewModel.onEvent(DealsEvent.EnteredDate(deal.date))
                            viewModel.onEvent(DealsEvent.SaveDeal)
                        }
                    ) {
                        Icon(
                            imageVector = Icons.Filled.Menu,
                            contentDescription = "Duplicate"
                        )
                    }

                    IconButton(
                        onClick = onDeleteClick
                    ) {
                        Icon(
                            imageVector = Icons.Filled.Delete,
                            contentDescription = "Delete"
                        )
                    }
                }
            }
        }
    }
}


@Composable
fun ShowModifierDeal(
    deal: Deal,
    openDialog: MutableState<Boolean>,
    viewModel: DealsViewModel
) {
    val NameVal = remember { mutableStateOf(deal.name) }
    val AmountVal = remember { mutableStateOf(deal.amount.toString()) }
    val date = Date(deal.date)
    val formatter = SimpleDateFormat("dd/MM/yyyy")
    val DateVal = remember { mutableStateOf(formatter.format(date)) }


    // DATE PICKER
    val year: Int
    val month: Int
    val day: Int

    val calendar = Calendar.getInstance()
    year = calendar.get(Calendar.YEAR)
    month = calendar.get(Calendar.MONTH)
    day = calendar.get(Calendar.DAY_OF_MONTH)
    calendar.time = Date()

    val datePickerDialog = DatePickerDialog(
        LocalContext.current,
        { _: DatePicker, year: Int, month: Int, dayOfMonth: Int ->
            DateVal.value = "$dayOfMonth/${month + 1}/$year"
        }, year, month, day
    )

    if (openDialog.value) {
        Dialog(onDismissRequest = { openDialog.value = false }) {
            Surface(
                modifier = Modifier
                    .width(300.dp)
                    .height(450.dp)
                    .padding(5.dp),
                shape = RoundedCornerShape(5.dp),
                color = Color.White
            ) {
                Column(
                    modifier = Modifier.padding(5.dp),
                    horizontalAlignment = Alignment.CenterHorizontally,
                    verticalArrangement = Arrangement.Center
                ) {
                    Spacer(modifier = Modifier.padding(5.dp))

                    Text(
                        text = "Modifier la transaction",
                        color = Color.Black,
                        fontWeight = FontWeight.Bold,
                        fontSize = 25.sp,
                        textAlign = TextAlign.Center
                    )

                    Spacer(modifier = Modifier.padding(10.dp))

                    OutlinedTextField(
                        value = NameVal.value,
                        onValueChange = {
                            NameVal.value = it
                        },
                        label = { Text(text = "Nom de la transaction") },
                        placeholder = { Text(text = "Inserer un nom...") },
                        singleLine = true,
                        modifier = Modifier.fillMaxWidth(0.8f)
                    )

                    Spacer(modifier = Modifier.padding(10.dp))

                    OutlinedTextField(
                        value = AmountVal.value,
                        onValueChange = {
                            AmountVal.value = it
                        },
                        label = { Text(text = "Montant de la transaction")},
                        keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Number),
                        singleLine = true,
                        modifier = Modifier.fillMaxWidth(0.8f)
                    )

                    Spacer(modifier = Modifier.padding(10.dp))

                    //DATE PICKER
                    Column(
                        verticalArrangement = Arrangement.Center,
                        horizontalAlignment = Alignment.CenterHorizontally

                    ) {

                        Text(text = "Date de la transaction : ${DateVal.value}")

                        Spacer(modifier = Modifier.size(16.dp))

                        Button(
                            onClick = {
                                datePickerDialog.show()
                            },
                            colors =
                            ButtonDefaults.buttonColors(backgroundColor = Color.Blue,
                                contentColor = Color.White
                            )
                        ) {
                            Text(text = "Séléctionner")
                        }
                    }

                    Spacer(modifier = Modifier.padding(10.dp))

                    Row() {
                        Button(
                            onClick = {
                                openDialog.value = false

                            },
                            modifier = Modifier
                                .weight(0.5f)
                                .height(60.dp)
                                .padding(10.dp),
                            shape = RoundedCornerShape(5.dp),
                            colors = ButtonDefaults.buttonColors(Color.Red)
                        ) {
                            Text(
                                text = "Fermer",
                                color = Color.White,
                                fontSize = 12.sp
                            )
                        }

                        Button(
                            onClick = {
                                openDialog.value = false
                                val milis = formatter.parse(DateVal.value).time
                                val amount = AmountVal.value.toFloat()*100
                                viewModel.onEvent(DealsEvent.EnteredAmount(amount.toInt()))
                                viewModel.onEvent(DealsEvent.EnteredDate(milis))
                                viewModel.onEvent(DealsEvent.EnteredName(NameVal.value))
                                viewModel.onEvent(DealsEvent.UpdateDeal(deal))
                            },
                            modifier = Modifier
                                .weight(0.5f)
                                .fillMaxWidth(0.5f)
                                .height(60.dp)
                                .padding(10.dp),
                            shape = RoundedCornerShape(5.dp),
                            colors = ButtonDefaults.buttonColors(Color.Green)
                        ) {
                            Text(
                                text = "Modifier",
                                color = Color.White,
                                fontSize = 12.sp
                            )
                        }
                    }
                }
            }
        }
    }
}