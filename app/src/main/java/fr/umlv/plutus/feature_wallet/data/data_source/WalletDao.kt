package fr.umlv.plutus.feature_wallet.data.data_source

import androidx.room.*
import fr.umlv.plutus.feature_wallet.domain.model.Wallet
import kotlinx.coroutines.flow.Flow

@Dao
interface WalletDao {

    @Query("SELECT * FROM wallet")
    fun getWallets(): Flow<List<Wallet>>

    @Query("SELECT * FROM wallet WHERE id = :id")
    suspend fun getWalletById(id: Int): Wallet?

    @Insert(onConflict = OnConflictStrategy.ABORT)
    suspend fun insertWallet(wallet: Wallet)

    @Delete
    suspend fun deleteWallet(wallet: Wallet)

    @Update
    suspend fun updateWallet(wallet: Wallet)
}