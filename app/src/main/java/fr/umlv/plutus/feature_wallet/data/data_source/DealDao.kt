package fr.umlv.plutus.feature_wallet.data.data_source

import androidx.room.*
import fr.umlv.plutus.feature_wallet.domain.model.Deal
import kotlinx.coroutines.flow.Flow

@Dao
interface DealDao {
    @Query("SELECT * FROM deal WHERE idWallet = :idWallet")
    fun getDealsByIdWallet(idWallet: Int): Flow<List<Deal>>

    @Query("SELECT * FROM deal WHERE id IN (:ids)")
    fun getDealsByIds(ids: Array<Int>): Flow<List<Deal>>

    @Query("SELECT MAX(id) FROM deal")
    suspend fun getLastDealId(): Int?

    @Insert(onConflict = OnConflictStrategy.ABORT)
    suspend fun insertDeal(deal: Deal)

    @Delete
    suspend fun deleteDeal(deal: Deal)

    @Update
    suspend fun updateDeal(deal: Deal)
}