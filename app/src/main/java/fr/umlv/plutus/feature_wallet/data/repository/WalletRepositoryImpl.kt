package fr.umlv.plutus.feature_wallet.data.repository

import fr.umlv.plutus.feature_wallet.data.data_source.WalletDao
import fr.umlv.plutus.feature_wallet.domain.model.Wallet
import fr.umlv.plutus.feature_wallet.domain.repository.WalletRepository
import kotlinx.coroutines.flow.Flow

class WalletRepositoryImpl(
    private val dao: WalletDao
) : WalletRepository {
    override fun getWallets(): Flow<List<Wallet>> {
        return dao.getWallets()
    }

    suspend override fun getWalletById(id: Int): Wallet? {
        return dao.getWalletById(id)
    }

    override suspend fun insertWallet(wallet: Wallet) {
        dao.insertWallet(wallet)
    }

    override suspend fun updateWallet(wallet: Wallet) {
        dao.updateWallet(wallet)
    }

    override suspend fun deleteWallet(wallet: Wallet) {
        dao.deleteWallet(wallet)
    }
}