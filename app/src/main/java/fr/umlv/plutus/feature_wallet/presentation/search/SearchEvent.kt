package fr.umlv.plutus.feature_wallet.presentation.search

import fr.umlv.plutus.feature_wallet.domain.model.Tag
import fr.umlv.plutus.feature_wallet.presentation.deals.DealsEvent


sealed class SearchEvent {
    data class EnteredTag(val tag: Tag): SearchEvent()
    data class EnteredTitle(val value: String) : SearchEvent()
    data class EnteredType(val value: String) : SearchEvent()
    object SaveTag : SearchEvent()
}