package fr.umlv.plutus.feature_wallet.domain.repository

import fr.umlv.plutus.feature_wallet.domain.model.Tag
import kotlinx.coroutines.flow.Flow

interface TagRepository {

    fun getTagsByIdDeal(idDeal: Int): Flow<List<Tag>>

    fun getTagsByName(title: String): Flow<List<Tag>>

    fun getTags(): Flow<List<Tag>>

    suspend fun insertTag(tag: Tag)

    suspend fun deleteTag(tag: Tag)
}