package fr.umlv.plutus.feature_wallet.data.repository

import fr.umlv.plutus.feature_wallet.data.data_source.TagDao
import fr.umlv.plutus.feature_wallet.domain.model.Tag
import fr.umlv.plutus.feature_wallet.domain.repository.TagRepository
import kotlinx.coroutines.flow.Flow

class TagRepositoryImpl(
    private val dao: TagDao
) : TagRepository {
    override fun getTagsByIdDeal(idDeal: Int): Flow<List<Tag>> {
        return dao.getTagsByIdDeal(idDeal)
    }

    override fun getTags(): Flow<List<Tag>> {
        return dao.getTags()
    }

    override fun getTagsByName(title: String): Flow<List<Tag>> {
        return dao.getTagByName(title)
    }

    override suspend fun insertTag(tag: Tag) {
        dao.insertTag(tag)
    }

    override suspend fun deleteTag(tag: Tag) {
        dao.deleteTag(tag)
    }
}