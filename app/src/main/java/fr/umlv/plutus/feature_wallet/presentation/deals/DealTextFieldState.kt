package fr.umlv.plutus.feature_wallet.presentation.deals

data class DealTextFieldState(
    val text: String = "",
    val hint: String = "",
    val isHintVisible: Boolean = true
)