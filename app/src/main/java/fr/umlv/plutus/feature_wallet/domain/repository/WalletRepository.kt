package fr.umlv.plutus.feature_wallet.domain.repository

import fr.umlv.plutus.feature_wallet.domain.model.Wallet
import kotlinx.coroutines.flow.Flow

interface WalletRepository {

    fun getWallets(): Flow<List<Wallet>>

    suspend fun getWalletById(id: Int): Wallet?

    suspend fun insertWallet(wallet: Wallet)

    suspend fun updateWallet(wallet: Wallet)

    suspend fun deleteWallet(wallet: Wallet)
}