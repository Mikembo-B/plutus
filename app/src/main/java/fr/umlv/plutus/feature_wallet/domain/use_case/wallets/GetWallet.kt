package fr.umlv.plutus.feature_wallet.domain.use_case.wallets

import fr.umlv.plutus.feature_wallet.domain.model.Wallet
import fr.umlv.plutus.feature_wallet.domain.repository.WalletRepository

class GetWallet(
    private val repository: WalletRepository
) {
    suspend operator fun invoke(id: Int): Wallet? {
        return repository.getWalletById(id)
    }
}