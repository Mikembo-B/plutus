package fr.umlv.plutus.feature_wallet.presentation.wallets

import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Home
import androidx.compose.material.icons.filled.Info
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.compose.ui.window.Dialog
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import fr.umlv.plutus.feature_wallet.presentation.util.Screen
import fr.umlv.plutus.feature_wallet.presentation.wallets.components.WalletItem

@ExperimentalAnimationApi
@Composable
fun WalletsScreen(
    navController: NavController,
    viewModel: WalletsViewModel = hiltViewModel()
) {
    val state = viewModel.state.value
    val scaffoldState = rememberScaffoldState()
    val openDialog = remember { mutableStateOf(false) }

    Scaffold(
        scaffoldState = scaffoldState,
        topBar = {
            TopAppBar(title = { Text("Plutus") }, backgroundColor = Color.Blue, contentColor = Color.White)
        },
        floatingActionButtonPosition = FabPosition.End,
        floatingActionButton = {
            ShowAddWallet(
                viewModel = viewModel,
                openDialog = openDialog
            )
            FloatingActionButton(
                onClick = {
                    openDialog.value = true
                },
            ) {
                Text("+")
            }
        },
        content = {
            ScrollableWallet(
                viewModel = viewModel,
                state = state,
                navController = navController
            )
        },
        bottomBar = {
            BottomAppBar(
                backgroundColor = Color.Blue,
                contentColor = Color.White
            ) {
                Row(
/*                    verticalAlignment = Alignment.CenterVertically,
                    horizontalArrangement = Arrangement.Center,*/
                    modifier = Modifier.fillMaxWidth()
                ) {
                    IconButton(modifier = Modifier
                        .weight(1f),
                        onClick = {
                            navController.navigate(Screen.WalletsScreen.route)
                        }
                    ) {
                        Icon(
                            imageVector = Icons.Filled.Home,
                            contentDescription = "Home"
                        )
                    }

                    IconButton(modifier = Modifier
                        .weight(1f),
                        onClick = {
                            navController.navigate(Screen.SearchScreen.route)
                        }
                    ) {
                        Icon(
                            imageVector = Icons.Filled.Info,
                            contentDescription = "Tag"
                        )
                    }
                }
            }
        }
    )
}

@ExperimentalAnimationApi
@Composable
fun ScrollableWallet(
    viewModel: WalletsViewModel,
    state: WalletsState,
    navController: NavController,
) {
    LazyColumn(modifier = Modifier.fillMaxSize()) {
        items(state.wallets) { wallet ->
            WalletItem(
                wallet = wallet,
                modifier = Modifier
                    .fillMaxWidth()
                    .clickable {
                        navController.navigate(
                            Screen.DealsScreen.route +
                                    "?walletId=${wallet.id}"
                        )
                    },
                onDeleteClick = {
                    viewModel.onEvent(WalletsEvent.DeleteWallet(wallet))
                }
            )
            Spacer(modifier = Modifier.height(16.dp))
        }
    }
}

@Composable
fun ShowAddWallet(
    openDialog: MutableState<Boolean>,
    viewModel: WalletsViewModel
) {
    val NameVal = remember { mutableStateOf("") }

    if (openDialog.value) {
        Dialog(onDismissRequest = { openDialog.value = false }) {
            Surface(
                modifier = Modifier
                    .width(300.dp)
                    .height(300.dp)
                    .padding(5.dp),
                shape = RoundedCornerShape(5.dp),
                color = Color.White
            ) {
                Column(
                    modifier = Modifier.padding(5.dp),
                    horizontalAlignment = Alignment.CenterHorizontally,
                    verticalArrangement = Arrangement.Center
                ) {
                    Spacer(modifier = Modifier.padding(5.dp))

                    Text(
                        text = "Ajouter un carnet de compte",
                        color = Color.Black,
                        fontWeight = FontWeight.Bold,
                        fontSize = 25.sp,
                        textAlign = TextAlign.Center
                    )

                    Spacer(modifier = Modifier.padding(10.dp))

                    OutlinedTextField(
                        value = NameVal.value,
                        onValueChange = {
                            NameVal.value = it
                            viewModel.onEvent(WalletsEvent.EnteredName(it))
                        },
                        label = { Text(text = "Nom de votre carnet") },
                        placeholder = { Text(text = "Inserer un nom...") },
                        singleLine = true,
                        modifier = Modifier.fillMaxWidth(0.8f)
                    )

                    Spacer(modifier = Modifier.padding(10.dp))

                    Row() {
                        Button(
                            onClick = {
                                openDialog.value = false

                            },
                            modifier = Modifier
                                .weight(0.5f)
                                .height(60.dp)
                                .padding(10.dp),
                            shape = RoundedCornerShape(5.dp),
                            colors = ButtonDefaults.buttonColors(Color.Red)
                        ) {
                            Text(
                                text = "Fermer",
                                color = Color.White,
                                fontSize = 12.sp
                            )
                        }

                        Button(
                            onClick = {
                                openDialog.value = false
                                //walletList.add(Wallet(NameVal.value))
                                viewModel.onEvent(WalletsEvent.SaveWallet)
                            },
                            modifier = Modifier
                                .weight(0.5f)
                                .fillMaxWidth(0.5f)
                                .height(60.dp)
                                .padding(10.dp),
                            shape = RoundedCornerShape(5.dp),
                            colors = ButtonDefaults.buttonColors(Color.Green)
                        ) {
                            Text(
                                text = "Ajouter",
                                color = Color.White,
                                fontSize = 12.sp
                            )
                        }
                    }
                }
            }
        }
    }
}
