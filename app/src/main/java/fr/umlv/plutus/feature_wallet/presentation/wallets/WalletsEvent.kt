package fr.umlv.plutus.feature_wallet.presentation.wallets

import androidx.compose.ui.focus.FocusState
import fr.umlv.plutus.feature_wallet.domain.model.Wallet

sealed class WalletsEvent {
    data class DeleteWallet(val wallet: Wallet) : WalletsEvent()
    data class EnteredName(val value: String) : WalletsEvent()
    data class ChangeNameFocus(val focusState: FocusState) : WalletsEvent()
    object SaveWallet : WalletsEvent()
}
