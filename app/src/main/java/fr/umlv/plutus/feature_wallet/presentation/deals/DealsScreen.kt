package fr.umlv.plutus.feature_wallet.presentation.deals

import android.app.DatePickerDialog
import android.content.Context
import android.widget.CalendarView
import android.widget.DatePicker
import android.widget.TimePicker
import androidx.appcompat.app.AppCompatActivity
import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.*
import androidx.compose.material.icons.outlined.ArrowDropDown
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Alignment.Companion.CenterVertically
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.compose.ui.viewinterop.AndroidView
import androidx.compose.ui.window.Dialog
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import com.google.android.material.datepicker.MaterialDatePicker
import dagger.hilt.internal.aggregatedroot.codegen._fr_umlv_plutus_PlutusApp
import fr.umlv.plutus.feature_wallet.domain.model.Tag
import fr.umlv.plutus.feature_wallet.presentation.deals.components.DealItem
import fr.umlv.plutus.feature_wallet.presentation.search.SearchEvent
import fr.umlv.plutus.feature_wallet.presentation.util.Screen
import fr.umlv.plutus.feature_wallet.presentation.wallets.WalletsEvent
import java.lang.reflect.Array.get
import java.text.SimpleDateFormat
import java.util.*
import kotlin.coroutines.coroutineContext

@ExperimentalAnimationApi
@Composable
fun DealsScreen(
    navController: NavController,
    viewModel: DealsViewModel = hiltViewModel()
) {
    val state = viewModel.state.value
    val scaffoldState = rememberScaffoldState()
    val openDialog = remember { mutableStateOf(false) }

    Scaffold(
        scaffoldState = scaffoldState,
        topBar = {
            TopAppBar(title = { Text(viewModel.walletName.value) }, backgroundColor = Color.Blue, contentColor = Color.White)
        },
        floatingActionButtonPosition = FabPosition.End,
        floatingActionButton = {
            ShowAddDeal(
                viewModel = viewModel,
                openDialog = openDialog
            )
            FloatingActionButton(
                onClick = {
                    openDialog.value = true
                },
            ) {
                Text("+")
            }
        },
        content = {
            ScrollableDeals(
                viewModel = viewModel,
                state = state,
                navController = navController
            )
        },
        bottomBar = {
            BottomAppBar(
                backgroundColor = Color.Blue,
                contentColor = Color.White
            ) {
                Row(
/*                    verticalAlignment = Alignment.CenterVertically,
                    horizontalArrangement = Arrangement.Center,*/
                    modifier = Modifier.fillMaxWidth()
                ) {
                    IconButton(modifier = Modifier
                        .weight(1f),
                        onClick = {
                            navController.navigate(Screen.WalletsScreen.route)
                        }
                    ) {
                        Icon(
                            imageVector = Icons.Filled.Home,
                            contentDescription = "Home"
                        )
                    }

                    IconButton(modifier = Modifier
                        .weight(1f),
                        onClick = {
                            navController.navigate(Screen.SearchScreen.route)
                        }
                    ) {
                        Icon(
                            imageVector = Icons.Filled.Info,
                            contentDescription = "Tag"
                        )
                    }
                }
            }
        }
    )
}

@ExperimentalAnimationApi
@Composable
fun ScrollableDeals(
    viewModel: DealsViewModel,
    state: DealsState,
    navController: NavController,
) {
    LazyColumn(modifier = Modifier.fillMaxSize()) {
        items(state.deals) { deal ->
            DealItem(
                deal = deal,
                modifier = Modifier
                    .fillMaxWidth()
                    /*.clickable {
                        navController.navigate(
                            Screen.DealsScreen.route +
                                    "?dealId=${deal.id}"
                        )
                    }*/,
                onDeleteClick = {
                    viewModel.onEvent(DealsEvent.DeleteDeal(deal))
                }
            )
            Spacer(modifier = Modifier.height(16.dp))
        }
    }
}

@Composable
fun ShowAddDeal(
    openDialog: MutableState<Boolean>,
    viewModel: DealsViewModel
) {
    val NameVal = remember { mutableStateOf("") }
    val AmountVal = remember { mutableStateOf("") }
    val DateVal = remember { mutableStateOf("") }

    val tags = viewModel.tagList.value
    var TagVal = remember { mutableStateOf(Tag(type = "=", title = viewModel.walletName.value)) }
    val formatter = SimpleDateFormat("dd/MM/yyyy")
    var expanded by remember { mutableStateOf(false) } // initial value

    // DATE PICKER
    val year: Int
    val month: Int
    val day: Int

    val calendar = Calendar.getInstance()
    year = calendar.get(Calendar.YEAR)
    month = calendar.get(Calendar.MONTH)
    day = calendar.get(Calendar.DAY_OF_MONTH)
    calendar.time = Date()

    val datePickerDialog = DatePickerDialog(
        LocalContext.current,
        { _: DatePicker, year: Int, month: Int, dayOfMonth: Int ->
            DateVal.value = "$dayOfMonth/${month + 1}/$year"
        }, year, month, day
    )

    if (openDialog.value) {
        Dialog(onDismissRequest = { openDialog.value = false }) {
            Surface(
                modifier = Modifier
                    .width(300.dp)
                    .height(550.dp)
                    .padding(5.dp),
                shape = RoundedCornerShape(5.dp),
                color = Color.White
            ) {
                Column(
                    modifier = Modifier.padding(5.dp),
                    horizontalAlignment = Alignment.CenterHorizontally,
                    verticalArrangement = Arrangement.Center
                ) {
                    Spacer(modifier = Modifier.padding(5.dp))

                    Text(
                        text = "Ajouter une transaction",
                        color = Color.Black,
                        fontWeight = FontWeight.Bold,
                        fontSize = 25.sp,
                        textAlign = TextAlign.Center
                    )

                    Spacer(modifier = Modifier.padding(10.dp))

                    OutlinedTextField(
                        value = NameVal.value,
                        onValueChange = {
                            NameVal.value = it
                            viewModel.onEvent(DealsEvent.EnteredName(it))
                        },
                        label = { Text(text = "Nom de la transaction") },
                        placeholder = { Text(text = "Inserer un nom...") },
                        singleLine = true,
                        modifier = Modifier.fillMaxWidth(0.8f)
                    )

                    Spacer(modifier = Modifier.padding(10.dp))

                    OutlinedTextField(
                        value = AmountVal.value,
                        onValueChange = {
                            AmountVal.value = it
                            /*viewModel.onEvent(DealsEvent.EnteredAmount(it.toInt()*100))*/
                        },
                        label = { Text(text = "Montant de la transaction")},
                        keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Number),
                        singleLine = true,
                        modifier = Modifier.fillMaxWidth(0.8f)
                    )

                    Spacer(modifier = Modifier.padding(10.dp))

                    //DATE PICKER
                    Column(
                        verticalArrangement = Arrangement.Center,
                        horizontalAlignment = Alignment.CenterHorizontally

                    ) {

                        Text(text = "Date de la transaction : ${DateVal.value}")

                        Spacer(modifier = Modifier.size(16.dp))

                        Button(
                            onClick = {
                                datePickerDialog.show()
                            },
                            colors =
                                ButtonDefaults.buttonColors(backgroundColor = Color.Blue,
                                contentColor = Color.White
                            )
                        ) {
                            Text(text = "Séléctionner")
                        }
                    }

                    Spacer(modifier = Modifier.padding(10.dp))

                    Box(
                        modifier = Modifier.fillMaxWidth(0.8f)
                    ) {
                        Column {
                            OutlinedTextField(
                                value = (TagVal.value.type+ " " +TagVal.value.title),
                                onValueChange = { },
                                label = { Text(text = "Choisir un tag") },
                                modifier = Modifier.fillMaxWidth(),
                                trailingIcon = { Icon(Icons.Outlined.ArrowDropDown, null) },
                                readOnly = true
                            )
                            DropdownMenu(
                                modifier = Modifier.fillMaxWidth(),
                                expanded = expanded,
                                onDismissRequest = { expanded = false },
                            ) {
                                tags.forEach { entry ->
                                    DropdownMenuItem(
                                        modifier = Modifier.fillMaxWidth(),
                                        onClick = {
                                            TagVal.value = entry
                                            expanded = false
                                        },
                                        content = {
                                            Text(
                                                text = entry.type+""+entry.title,
                                                modifier = Modifier
                                                    .wrapContentWidth()
                                            )
                                        }
                                    )
                                }
                            }
                        }

                        Spacer(
                            modifier = Modifier
                                .matchParentSize()
                                .background(Color.Transparent)
                                .padding(10.dp)
                                .clickable(
                                    onClick = { expanded = !expanded }
                                )
                        )
                    }

                    Spacer(modifier = Modifier.padding(10.dp))


                    Row() {
                        Button(
                            onClick = {
                                openDialog.value = false

                            },
                            modifier = Modifier
                                .weight(0.5f)
                                .height(60.dp)
                                .padding(10.dp),
                            shape = RoundedCornerShape(5.dp),
                            colors = ButtonDefaults.buttonColors(Color.Red)
                        ) {
                            Text(
                                text = "Fermer",
                                color = Color.White,
                                fontSize = 12.sp
                            )
                        }

                        Button(
                            onClick = {
                                openDialog.value = false
                                val milis = formatter.parse(DateVal.value).time
                                val amount = AmountVal.value.toFloat()*100
                                viewModel.onEvent(DealsEvent.EnteredAmount(amount.toInt()))
                                viewModel.onEvent(DealsEvent.EnteredDate(milis))
                                viewModel.onEvent(DealsEvent.SaveDeal)
                                viewModel.onEvent(DealsEvent.EnteredTitle(TagVal.value.title))
                                viewModel.onEvent(DealsEvent.EnteredType(TagVal.value.type))
                                viewModel.onEvent(DealsEvent.SaveTag)
                            },
                            modifier = Modifier
                                .weight(0.5f)
                                .fillMaxWidth(0.5f)
                                .height(60.dp)
                                .padding(10.dp),
                            shape = RoundedCornerShape(5.dp),
                            colors = ButtonDefaults.buttonColors(Color.Green)
                        ) {
                            Text(
                                text = "Ajouter",
                                color = Color.White,
                                fontSize = 12.sp
                            )
                        }
                    }
                }
            }
        }
    }
}

