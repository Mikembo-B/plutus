package fr.umlv.plutus.feature_wallet.presentation.wallets

import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import fr.umlv.plutus.feature_wallet.domain.model.Wallet
import fr.umlv.plutus.feature_wallet.domain.use_case.PlutusUseCases
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.asSharedFlow
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class WalletsViewModel @Inject constructor(
    private val plutusUseCases: PlutusUseCases,
) : ViewModel() {
    private val _state = mutableStateOf(WalletsState())
    private val _walletName = mutableStateOf(
        WalletTextFieldState(
            hint = "Enter name..."
        )
    )
    private val _eventFlow = MutableSharedFlow<UiEvent>()

    val state: State<WalletsState> = _state
    val walletName: State<WalletTextFieldState> = _walletName
    val eventFlow = _eventFlow.asSharedFlow()

    private var getWalletsJob: Job? = null

    init {
        getWallets()
    }

    fun onEvent(event: WalletsEvent) {
        when (event) {
            is WalletsEvent.DeleteWallet -> {
                viewModelScope.launch {
                    plutusUseCases.deleteWallet(event.wallet)
                }
            }
            is WalletsEvent.EnteredName -> {
                _walletName.value = walletName.value.copy(
                    text = event.value
                )
            }
            is WalletsEvent.ChangeNameFocus -> {
                //  Do nothing
            }
            is WalletsEvent.SaveWallet -> {
                viewModelScope.launch {
                    plutusUseCases.addWallet(
                        Wallet(
                            name = walletName.value.text,
                        )
                    )
                    _eventFlow.emit(UiEvent.SaveWallet)
                }
            }
        }
    }

    private fun getWallets() {
        getWalletsJob?.cancel()
        getWalletsJob = plutusUseCases.getWallets()
            .onEach { wallets ->
                _state.value = state.value.copy(
                    wallets = wallets
                )
            }
            .launchIn(viewModelScope)
    }

    sealed class UiEvent {
        object SaveWallet : UiEvent()
    }
}