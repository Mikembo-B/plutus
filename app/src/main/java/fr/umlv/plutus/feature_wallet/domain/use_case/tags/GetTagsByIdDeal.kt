package fr.umlv.plutus.feature_wallet.domain.use_case.tags

import fr.umlv.plutus.feature_wallet.domain.model.Tag
import fr.umlv.plutus.feature_wallet.domain.repository.TagRepository
import kotlinx.coroutines.flow.Flow

class GetTagsByIdDeal(
    private val repository: TagRepository
) {
    operator fun invoke(idDeal: Int): Flow<List<Tag>> {
        return repository.getTagsByIdDeal(idDeal)
    }
}