package fr.umlv.plutus.feature_wallet.data.data_source

import androidx.room.*
import fr.umlv.plutus.feature_wallet.domain.model.Tag
import kotlinx.coroutines.flow.Flow

@Dao
interface TagDao {
    @Query("SELECT * FROM tag WHERE idDeal = :idDeal")
    fun getTagsByIdDeal(idDeal: Int): Flow<List<Tag>>

    @Query("SELECT * FROM tag WHERE title LIKE :title")
    fun getTagByName(title: String): Flow<List<Tag>>

    @Query("SELECT * FROM tag")
    fun getTags(): Flow<List<Tag>>

    @Insert(onConflict = OnConflictStrategy.ABORT)
    suspend fun insertTag(tag: Tag)

    @Delete
    suspend fun deleteTag(tag: Tag)
}