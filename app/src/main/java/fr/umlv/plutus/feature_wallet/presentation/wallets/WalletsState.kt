package fr.umlv.plutus.feature_wallet.presentation.wallets

import fr.umlv.plutus.feature_wallet.domain.model.Wallet

data class WalletsState(
    val wallets: List<Wallet> = emptyList()
)