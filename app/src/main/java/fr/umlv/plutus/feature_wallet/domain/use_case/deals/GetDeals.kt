package fr.umlv.plutus.feature_wallet.domain.use_case.deals

import fr.umlv.plutus.feature_wallet.domain.model.Deal
import fr.umlv.plutus.feature_wallet.domain.repository.DealRepository
import kotlinx.coroutines.flow.Flow

class GetDeals(
    private val repository: DealRepository
) {
     operator fun invoke(idWallet: Int): Flow<List<Deal>> {
        return repository.getDealsByIdWallet(idWallet)
    }
}