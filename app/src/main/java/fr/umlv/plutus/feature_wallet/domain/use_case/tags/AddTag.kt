package fr.umlv.plutus.feature_wallet.domain.use_case.tags

import fr.umlv.plutus.feature_wallet.domain.model.Tag
import fr.umlv.plutus.feature_wallet.domain.repository.TagRepository

class AddTag(
    private val repository: TagRepository
) {
    suspend operator fun invoke(tag: Tag) {
        repository.insertTag(tag)
    }
}