package fr.umlv.plutus.feature_wallet.presentation.wallets

data class WalletTextFieldState(
    val text: String = "",
    val hint: String = "",
    val isHintVisible: Boolean = true
)
