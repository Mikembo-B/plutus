package fr.umlv.plutus.feature_wallet.presentation.search

import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import fr.umlv.plutus.feature_wallet.domain.model.Tag
import fr.umlv.plutus.feature_wallet.domain.use_case.PlutusUseCases
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class SearchViewModel @Inject constructor(
    val plutusUseCases: PlutusUseCases
) : ViewModel() {

    private val _state = mutableStateOf(SearchState())
    val state: State<SearchState> = _state

    private var _tags = emptyList<Tag>()
    private val _eventFlow = MutableSharedFlow<UiEvent>()

    private val _tagTitle = mutableStateOf("")
    private val _tagType = mutableStateOf("")

    val tagTitle: State<String> = _tagTitle
    val tagType: State<String> = _tagType

    private val _tagList = mutableStateOf(emptyList<Tag>())
    val tagList: State<List<Tag>> = _tagList


    init {
        plutusUseCases.getTags().onEach {
            _tagList.value = it.distinct()
        }.launchIn(viewModelScope)
    }

    fun onEvent(event: SearchEvent) {
        when (event) {
            is SearchEvent.EnteredTag -> {
                plutusUseCases.getTagsByName(event.tag.title).onEach { tags ->
                    _tags = tags
                }.launchIn(viewModelScope)
                val dealIds = _tags.map {
                    it.idDeal
                }.toTypedArray()
                plutusUseCases.getDealsByIds(dealIds).onEach {
                    _state.value = state.value.copy(
                        deals = it
                    )
                }
            }
            is SearchEvent.EnteredTitle -> {
                _tagTitle.value = event.value
            }
            is SearchEvent.EnteredType -> {
                _tagType.value = event.value
            }
            is SearchEvent.SaveTag -> {
                viewModelScope.launch {
                    plutusUseCases.addTag(
                        Tag(
                            title = tagTitle.value,
                            type = tagType.value
                        )
                    )
                    _eventFlow.emit(UiEvent.SaveTag)
                }
            }
        }
    }

    sealed class UiEvent {
        object SaveTag : UiEvent()
    }
}