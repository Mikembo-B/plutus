package fr.umlv.plutus.feature_wallet.presentation.search

import androidx.compose.ui.focus.FocusState
import fr.umlv.plutus.feature_wallet.domain.model.Tag

sealed class TagEvent {
    data class DeleteTag(val tag: Tag) : TagEvent()
    data class EnteredName(val value: String) : TagEvent()
    data class ChangeNameFocus(val focusState: FocusState) : TagEvent()
    object SaveTag : TagEvent()
}