package fr.umlv.plutus.feature_wallet.domain.repository

import fr.umlv.plutus.feature_wallet.domain.model.Deal
import kotlinx.coroutines.flow.Flow

interface DealRepository {

    fun getDealsByIdWallet(idWallet: Int): Flow<List<Deal>>

    fun getDealsByIds(ids: Array<Int>): Flow<List<Deal>>

    suspend fun getLastDealId(): Int?

    suspend fun insertDeal(deal: Deal)

    suspend fun deleteDeal(deal: Deal)

    suspend fun updateDeal(deal: Deal)
}