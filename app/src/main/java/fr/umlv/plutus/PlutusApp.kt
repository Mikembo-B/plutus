package fr.umlv.plutus

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class PlutusApp : Application()