package fr.umlv.plutus.di

import android.app.Application
import androidx.room.Room
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import fr.umlv.plutus.feature_wallet.data.data_source.PlutusDatabase
import fr.umlv.plutus.feature_wallet.data.repository.DealRepositoryImpl
import fr.umlv.plutus.feature_wallet.data.repository.TagRepositoryImpl
import fr.umlv.plutus.feature_wallet.data.repository.WalletRepositoryImpl
import fr.umlv.plutus.feature_wallet.domain.repository.DealRepository
import fr.umlv.plutus.feature_wallet.domain.repository.TagRepository
import fr.umlv.plutus.feature_wallet.domain.repository.WalletRepository
import fr.umlv.plutus.feature_wallet.domain.use_case.*
import fr.umlv.plutus.feature_wallet.domain.use_case.deals.*
import fr.umlv.plutus.feature_wallet.domain.use_case.tags.*
import fr.umlv.plutus.feature_wallet.domain.use_case.wallets.*
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object AppModule {

    @Provides
    @Singleton
    fun providePlutusDatabase(app: Application): PlutusDatabase {
        return Room.databaseBuilder(
            app,
            PlutusDatabase::class.java,
            PlutusDatabase.DATABASE_NAME
        ).build()
    }

    @Provides
    @Singleton
    fun provideWalletRepository(db: PlutusDatabase): WalletRepository {
        return WalletRepositoryImpl(db.walletDao)
    }

    @Provides
    @Singleton
    fun provideDealRepository(db: PlutusDatabase): DealRepository {
        return DealRepositoryImpl(db.dealDao)
    }

    @Provides
    @Singleton
    fun provideTagRepository(db: PlutusDatabase): TagRepository {
        return TagRepositoryImpl(db.tagDao)
    }

    @Provides
    @Singleton
    fun providePlutusUseCases(
        walletRepository: WalletRepository,
        dealRepository: DealRepository,
        tagRepository: TagRepository
    ): PlutusUseCases {
        return PlutusUseCases(
            addWallet = AddWallet(walletRepository),
            getWallets = GetWallets(walletRepository),
            getWallet = GetWallet(walletRepository),
            deleteWallet = DeleteWallet(walletRepository),
            getDeals = GetDeals(dealRepository),
            addDeal = AddDeal(dealRepository),
            deleteDeal = DeleteDeal(dealRepository),
            getTagsByIdDeal = GetTagsByIdDeal(tagRepository),
            addTag = AddTag(tagRepository),
            deleteTag = DeleteTag(tagRepository),
            updateDeal = UpdateDeal(dealRepository),
            updateWallet = UpdateWallet(walletRepository),
            getTagsByName = GetTagsByName(tagRepository),
            getDealsByIds = GetDealsByIds(dealRepository),
            getTags = GetTags(tagRepository),
            getLastDealId = GetLastDealId(dealRepository)
        )
    }
}