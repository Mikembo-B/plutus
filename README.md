# Changelog
Tous les changements notables apportés à ce projet seront documentés dans ce fichier.

## [Unreleased]
- Ajouter une transaction dans un carnet de compte
- Accéder aux transactions d'un carnet de compte
- Modifier les données d'une transaction
- Dupliquer une transaction
- Ajouter des tags
- Créer une page de recherche
- Mettre en place une base de données

## [1.0.0] - 03-06-2022
### Added
- Ajouter des carnets de compte
- Ajouter une transaction dans un carnet de compte (nom et montant)
- Accéder aux transactions d'un carnet de compte

## [1.1.0] - 06-06-2022
### Added
- Mettre en place une base de données
- Ajouter une transaction dans un carnet de compte (complet sauf tags)

## [1.2.0] - 08-06-2022
### Added
- Ajouter des tags
- Ajouter une transaction dans un carnet de compte (complet)

## [1.3.0] - 12-06-2022
### Added
- Modifier les données d'une transaction
- Dupliquer une transaction

